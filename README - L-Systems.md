Tous les scripts sont situés dans Assets/Scripts/LSystem_Proto
Seul LSystemProto.cs est réellement interessant il contient le parseur et l'interpreteur du LSystem.
La scene LSystemScene contient quelques exemples de formes simples réalisées avec mon outil, il suffit d'activer les gameobjects desactivés et d'appuyer sur le boutton generate
ou generate mesh.


L'outil est encore tres experimental, il est assez fragile, peu optimisé et as encore quelques bugs (en particulier sur avec les operations arithmetiques sur les nombres a virgule).
Il faut faire attention lors de son utilisation (en particulier lorsque le mode dynamic est activé) car un mauvais parametrage peut facilement demander une operation relativement longue a l'interpreteur et par conséquent faire freeze unity.

Les 2 principales difficultées recontrées lors de ce projet sont lié au developpement de l'outil. La premiere (que je n'ai pas vraiment su surmonter) étant la problématique
comment concevoir un outil simple a utiliser sans trop contraindre les possibilitées.
La seconde étant simplement le developpement de l'outil qui est actuellement tres brouillon faute d'avoir pu résoudre le premier probleme.

Avec le recul que j'ai actuellement, je pense qu'un domain specific language serai le type d'outil le plus adapté pour implémenter un tel systeme. 
Cependant avec la contrainte de temps que nous avions cela n'aurais pas été faisable.

J'ai écris sur mon blog un résumé de mon travail : 
https://blackbird806.github.io/LittleBirdBlog/2020/03/A-Unity-Tool-for-L-Systems
https://blackbird806.github.io/LittleBirdBlog/2020/02/A-short-introduction-to-L-Systems
