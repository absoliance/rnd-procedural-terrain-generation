﻿using UnityEngine;

[CreateAssetMenu(fileName = "Weather", menuName = "Climate system/Weather", order = 1)]
public class Weather : ScriptableObject
{
	#region Unity methods
	public override string ToString()
	{
		return name;
	}

	#endregion

	#region Members

	public ParticleSystem Effect;
	public float Exposure;

	#endregion
}
