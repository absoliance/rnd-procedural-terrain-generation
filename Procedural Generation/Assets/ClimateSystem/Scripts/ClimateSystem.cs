﻿using System.Collections.Generic;
using UnityEditor.EditorTools;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using Vector3 = UnityEngine.Vector3;

[RequireComponent(typeof(Volume))]
public class ClimateSystem : MonoBehaviour
{
	#region Unity methods

	private void Awake()
	{
		var volume = GetComponent<Volume>();

		weather_particle_system = weather_effect?.GetComponent<ParticleSystem>();

		if (!volume.profile.TryGet(out white_balance))
			Debug.LogError("White balance not found in Volume profile!");

		if (!volume.profile.TryGet(out exposure))
			Debug.LogError("Exposure not found in Volume profile!");

		if (player_transform == null)
			player_transform = GameObject.FindGameObjectWithTag("Player")?.transform;
	}

	private void Update()
	{
		current_climate = GetClimate(player_transform?.position);

		current_weather = GetWeatherFromCurrentClimate();

		SetVisualEnvironment();
	}

	#endregion

	#region Exposed methods

	// Used in editor preview
	public Texture2D GetClimatePreview(Vector2Int texture_size, float x, float y)
	{
		Texture2D texture = new Texture2D(texture_size.x, texture_size.y);

		for (int i = 0; i < texture_size.x; i++)
		{
			for (int j = 0; j < texture_size.y; j++)
			{
				var climate = GetClimate(new Vector3(x + i - texture_size.x / 2 , 0, y + j - texture_size.y / 2));

				texture.SetPixel(i, j, climate.PreviewColor);
			}
		}

		texture.Apply();

		return texture;
	}

	// Used in editor preview
	public Vector2 GetPlayer2DPosition()
	{
		return player_transform == null ? Vector2.zero : new Vector2(player_transform.position.x, player_transform.position.z);
	}

	#endregion

	#region Methods

	private Climate GetClimate(Vector3? position)
	{
		if (climates == null || ClimateCount == 0 || position == null)
			return null;

		float x = position.Value.x / climate_noise_size * climate_noise_scale;
		float y = position.Value.z / climate_noise_size * climate_noise_scale;

		int index = Mathf.Clamp((int) (Mathf.PerlinNoise(x + seed, y + seed) * ClimateCount), 0, ClimateCount);

		if (index >= ClimateCount)
			index = ClimateCount - 1;

		return climates[index];
	}

	private Weather GetWeatherFromCurrentClimate()
	{
		if (current_climate == null || current_climate.Weathers == null || current_climate.Weathers.Count == 0)
			return null;

		var weathers = current_climate.Weathers;

		float wind_speed = Random.Range(0, current_climate.MaxWindSpeed);

		float x = (Time.time + wind_speed) / weather_noise_size * weather_noise_scale;
		float y = -(Time.time + wind_speed) / weather_noise_size * weather_noise_scale;

		int index = Mathf.Clamp((int)(Mathf.PerlinNoise(x + seed, y + seed) * weathers.Count), 0, weathers.Count);

		return weathers[index];
	}

	private void SetVisualEnvironment()
	{
		if (!current_climate || !current_weather || !exposure || !white_balance)
			return;

		exposure.fixedExposure.value = current_weather.Exposure;

		white_balance.temperature.value = GetTemperatureFromClimate(current_climate);

		ApplyWeatherParticles();
	}

	private float GetTemperatureFromClimate(Climate climate)
	{
		if (climate == null)
			return 0.0f;

		float value = Time.time * temperature_variance;

		return Mathf.Lerp(climate.MinTemperature, climate.MaxTemperature, Mathf.PerlinNoise(value + seed, value + seed));
	}

	private void ApplyWeatherParticles()
	{
		if (!current_weather)
			return;

		transform.position = player_transform.position;

		if (current_weather.Effect)
		{
			weather_effect.SetActive(true);

			var effect = current_weather.Effect;

			weather_effect.transform.localPosition = current_weather.Effect.transform.position;
			weather_effect.transform.localRotation = current_weather.Effect.transform.rotation;

			UnityEditorInternal.ComponentUtility.CopyComponent(effect);

			UnityEditorInternal.ComponentUtility.PasteComponentValues(weather_particle_system);
		}
		else
			weather_effect.SetActive(false);
	}

	#endregion

	#region Properties

	public int ClimateCount => climates.Count;

	#endregion

	#region Members

	[Header("Climate parameters")]
	[SerializeField] private List<Climate> climates;
	[SerializeField, Range(0.01f, 0.5f)] private float temperature_variance = 0.25f;
	
	[Header("Noise parameters")]
	[SerializeField] private int seed;
	[SerializeField, Min(1)] private float climate_noise_size;
	[SerializeField, Min(0.01f)] private float climate_noise_scale;
	[SerializeField, Min(1)] private float weather_noise_size;
	[SerializeField, Min(0.01f)] private float weather_noise_scale;

	[Header("Other parameters")]
	[SerializeField] private Transform player_transform;
	[SerializeField] private GameObject weather_effect;

	private Climate current_climate;
	private Weather current_weather;

	private ParticleSystem weather_particle_system;

	private WhiteBalance white_balance;
	private Exposure exposure;

	#endregion

}
