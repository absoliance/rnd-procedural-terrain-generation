﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Climate", menuName = "Climate system/Climate", order = 0)]
public class Climate : ScriptableObject
{
	#region Unity methods
	public override string ToString()
	{
		return name;
	}

	#endregion

	#region Members

	public List<Weather> Weathers;

	[Header("Climate general settings")]
	public float MinTemperature;
	public float MaxTemperature;
	public float MaxWindSpeed;

	[Header("Preview settings")]
	public Color PreviewColor;

	#endregion
}
