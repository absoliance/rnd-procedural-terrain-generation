﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Random = UnityEngine.Random;

namespace UnityLsystem
{
	[Serializable]
	struct Production
	{
		[SerializeField]
		public string expanded;
		[SerializeField, Range(0, 1)]
		public float probability;
	}

	class Axiom
	{
		public Axiom()
		{
			parameters = new List<float>();
		}

		public Axiom(char val, List<float> p)
		{
			axiom = val;
			parameters = p;
		}

		public Axiom(char val) : this()
		{
			axiom = val;
		}

		public char axiom;
		public List<float> parameters;
	}

	[Serializable]
	class Rule
	{
		[SerializeField]
		public string axiom;

		// optionnal paramters
		[SerializeField]
		private List<string> paramNames;

		// list of productions
		// the sum of production.probability must be 1 in order to works properly
		[SerializeField]
		public Production[] productions;

		// ignored chars of context sensitive L-System
		[SerializeField] public string ignoreList;
		// chars needed on the left to be evaluated to true
		[SerializeField] public string preConditionLeft;
		// chars needed on the right to be evaluated to true
		[SerializeField] public string preConditionRight;

		// simple precondition system
		public bool CheckCondition(List<Axiom> pAxioms, int index)
		{
			bool conditionsChecked = true;

			if (preConditionLeft.Length > 0)
			{
				int leftCheck = 1;
				while (index - leftCheck >= 0 && ignoreList.Contains(pAxioms[index - leftCheck].axiom)) leftCheck++;
				conditionsChecked &= index - leftCheck >= 0 && preConditionLeft.Contains(pAxioms[index - leftCheck].axiom);
			}
			if (preConditionRight.Length > 0)
			{
				int rightCheck = 1;
				while (index + rightCheck < pAxioms.Count && ignoreList.Contains(pAxioms[index + rightCheck].axiom)) rightCheck++;
				conditionsChecked &= index + 1 < pAxioms.Count && preConditionRight.Contains(pAxioms[index + rightCheck].axiom);
			}
			return conditionsChecked;
		}

		// https://stackoverflow.com/questions/4463561/weighted-random-selection-from-array
		public string GetProbabilityExpansion(List<float> parameterValues)
		{
			if (productions.Length == 0)
				return "";

			float rnd = Random.Range(0.0f, 1.0f);
			List<float> cdf = new List<float> { productions[0].probability };
			for (int i = 1; i < productions.Length; i++)
			{
				cdf.Add(cdf.Last() + productions[i].probability);
			}

			uint index = 0;
			foreach (var value in cdf)
			{
				if (rnd < value)
					break;
				index++;
			}

			index %= (uint)productions.Length;
			int j = 0;
			string result = productions[index].expanded;
			foreach (var paramName in paramNames)
			{
				if (j >= parameterValues.Count)
					break;
				result = result.Replace(paramName, parameterValues[j].ToString());
				j++;
			}

			return result;
		}
	}

	public struct Line
	{
		public Line(Vector3 s, Vector3 e)
		{
			start = s;
			end = e;
		}
		public Vector3 start;
		public Vector3 end;
	}

	// properties of a segment of the L-System, only line are really userfull for now
	public struct Property
	{
		public Line line;
		public State state;
		public float width;
	}

	// state of the cursor (turtle) used for branching
	public struct State
	{
		public Vector3 pos;
		public Quaternion dir;
		public float width;
	}

	public class LSystemProto : MonoBehaviour
	{
		// seed used by the stochastic L-System
		[SerializeField] private int seed;

		// enable this to interpret L-System as you type.
		// some interpretations errors will likely be send when writing a rule (because of incomplete/invalid syntax) just ignore them
		// until you finished what you want
		// this option can use a lot of performances since it re interpret the L-System each gizmo draw call
		// consider using the button generate instead
		[SerializeField] public bool dynamicGeneration;
		[SerializeField] private Color gizmoColor;
		
		[Header("rules")]
		// the production rules (P)
		[SerializeField] private Rule[] rules;

		[Header("variables")]
		// variables are a set of constants (C) that can be used in the L systems
		[SerializeField] private string[] variablesNames;
		[SerializeField] private float[] variablesValues;

		// special constants used for the cursor(turtle) movment
		[SerializeField] private float defaultStepLength;
		[SerializeField] private float defaultWidth;
		[SerializeField, Range(0.0f, 180.0f)] private float defaultAngle;


		[SerializeField, Range(0, 10)] private uint iterations;
		// this is omega the initial state
		[SerializeField] private string initialSystem;
		// this is the final rewrited system
		[SerializeField, TextArea] public string path;

		[SerializeField] private float meshSize;
		[SerializeField, Range(3, 10)] private int meshPrecision;

		private List<Property> properties;
		private Stack<State> SavedStates;

		// parser vars
		private Vector3 lastPos;
		private State currentState;
		private bool isInParamList;
		private int parserIndex;
		private List<Axiom> parsedAxioms;
		private Axiom currentAxiom;

		// https://stackoverflow.com/a/6052679
		// used to evaluate arithmetic expression when writing a production rule or the initial state
		public static float Evaluate(string expression)
		{
			System.Data.DataTable table = new System.Data.DataTable();
			table.Columns.Add("expression", string.Empty.GetType(), expression);
			System.Data.DataRow row = table.NewRow();
			table.Rows.Add(row);
			return float.Parse((string)row["expression"]);
		}

		void InterpretAxiom(Axiom current)
		{
			// default arguments for parametric L-Systems
			float stepLength = (current.parameters.Count > 0 ? current.parameters[0] : defaultStepLength);
			float angle = (current.parameters.Count > 0 ? current.parameters[0] : defaultAngle);
			float width = (current.parameters.Count > 0 ? current.parameters[0] : defaultWidth);

			// special key axioms that are instruction for the cursor(turtle)
			switch (current.axiom)
			{
				case 'F':
					currentState.pos += currentState.dir * Vector3.up * stepLength;
					Property prop = new Property
					{
						line = new Line(lastPos, currentState.pos),
						state = currentState
					};
					properties.Add(prop);
					break;
				case '[':
					SavedStates.Push(currentState);
					break;
				case ']':
					currentState = SavedStates.Pop();
					break;
				case 'f':
					currentState.pos += currentState.dir * Vector3.up * stepLength;
					break;
				case '+':
					currentState.dir *= Quaternion.Euler(0.0f, angle, 0.0f);
					break;
				case '-':
					currentState.dir *= Quaternion.Euler(0.0f, -angle, 0.0f);
					break;
				case '&':
					currentState.dir *= Quaternion.Euler(angle, 0.0f, 0.0f);
					break;
				case '^':
					currentState.dir *= Quaternion.Euler(-angle, 0.0f, 0.0f);
					break;
				case '/':
					currentState.dir *= Quaternion.Euler(0.0f, 0.0f, angle);
					break;
				case '\\':
					currentState.dir *= Quaternion.Euler(0.0f, 0.0f, -angle);
					break;
				case '|':
					currentState.dir *= Quaternion.Euler(0.0f, 180.0f, 0.0f);
					break;
				case '!':
					currentState.width = width;
					break;
				case '(':
					isInParamList = true;
					parserIndex++;
					return;
			}

			lastPos = currentState.pos;
		}

		void Interpret()
		{
			foreach (var axiom in parsedAxioms)
			{
				InterpretAxiom(axiom);
			}
		}

		void ParseActionOrParamListStart()
		{
			var current = path[parserIndex];
			currentAxiom = new Axiom(current);

			if (parserIndex + 1 < path.Length && path[parserIndex + 1] == '(')
			{
				isInParamList = true;
				parserIndex++;
			}
			else 
				parsedAxioms.Add(currentAxiom);

			parserIndex++;
		}

		void ParseParameters()
		{
			string strParams = path.Substring(parserIndex, path.IndexOf(')', parserIndex) - parserIndex);
			var parameters = Array.ConvertAll(strParams.Split(','), Evaluate);
			currentAxiom.parameters.AddRange(parameters.ToList());
			parserIndex += strParams.Length+1;
			isInParamList = false;
			parsedAxioms.Add(currentAxiom);
		}

		void Parse()
		{
			parserIndex = 0;
			isInParamList = false;
			parsedAxioms = new List<Axiom>();
			
			ReplaceVariables();
			while (parserIndex < path.Length)
			{
				if (isInParamList)
					ParseParameters();
				else
					ParseActionOrParamListStart();
			}
		}

		string GenerateNewPath(string inPath)
		{
			// newPath is likely to be at least as long as inPath
			// however this is not always true
			StringBuilder newPath = new StringBuilder(inPath.Length);
			int nAxiom = 0;
			foreach (var axiom in parsedAxioms)
			{
				foreach (var rule in rules)
				{
					if (axiom.axiom.ToString() == rule.axiom && rule.CheckCondition(parsedAxioms, nAxiom))
					{
						newPath.Append(rule.GetProbabilityExpansion(axiom.parameters));
						goto match; // an axiom can only be matched once
					}
				}

				// no match
				newPath.Append(axiom.axiom);
				if (axiom.parameters.Count > 0)
				{
					newPath.Append('(');
					for (int i = 0; i < axiom.parameters.Count - 1; i++)
					{
						newPath.Append(axiom.parameters[i]).Append(',');
					}
					newPath.Append(axiom.parameters.Last()).Append(')');
				}

				match:
				nAxiom++;
			}

			return newPath.ToString();
		}

		void ReplaceVariables()
		{
			if (variablesNames.Length != variablesValues.Length)
			{
				Debug.LogError("variablesNames and variablesValues must have the same size !");
				return;
			}

			for(int i = 0; i < variablesNames.Length; i++)
			{
				path = path.Replace(variablesNames[i], variablesValues[i].ToString());
			}
		}

		public void Generate()
		{
			properties = new List<Property>();
			SavedStates = new Stack<State>();

			path = initialSystem;

			Random.InitState(seed);

			for (uint i = 0; i < iterations; i++)
			{
				Parse();
				path = GenerateNewPath(path);
			}

			currentState = new State
			{
				pos = transform.position,
				dir = transform.rotation
			};

			lastPos = transform.position;
			Parse();
			Interpret();
		}

		public void GenerateMesh()
		{
			Generate();
			MeshFilter meshFilter = gameObject.GetComponent<MeshFilter>();
			if (meshFilter == null)
			{
				meshFilter = gameObject.AddComponent<MeshFilter>();
			}
			Vector3[] points = new Vector3[properties.Count * 2];
			int nPts = 0;
			for (int i = 0; i < properties.Count; i++)
			{
				points[nPts] = properties[i].line.start;
				points[nPts+1] = properties[i].line.end;
				nPts += 2;
			}
			meshFilter.sharedMesh = MeshTreeGenerator.GenerateTreeMesh(points, meshPrecision, meshSize);
		}

		void OnDrawGizmos()
		{
			if (dynamicGeneration)
				Generate();
			Gizmos.color = gizmoColor;
			foreach (var prop in properties)
			{
				Gizmos.DrawLine(prop.line.start, prop.line.end);
			}
		}
	}
}

