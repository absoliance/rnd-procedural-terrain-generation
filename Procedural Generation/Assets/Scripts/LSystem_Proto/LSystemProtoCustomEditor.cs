﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityLsystem;

[CustomEditor(typeof(LSystemProto))]
public class LSystemProtoCustomEditor : Editor
{
	void OnEnable()
	{
	}

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		serializedObject.Update();
		serializedObject.ApplyModifiedProperties();
		LSystemProto lSystem = (LSystemProto)target;

		if(GUILayout.Button("Generate"))
		{
			lSystem.Generate();
		}

		if (GUILayout.Button("Generate mesh"))
		{
			lSystem.GenerateMesh();
		}

		if (GUILayout.Button("ClearMesh"))
		{
			lSystem.GetComponent<MeshFilter>().sharedMesh = null;
		}
	}
}
