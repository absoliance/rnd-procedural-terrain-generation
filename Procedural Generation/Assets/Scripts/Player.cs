﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	[HideInInspector]
	public Rigidbody rb;
	private CameraController cameraController;

	public float acceleration = 35.0f;
	public float jumpForce = 500.0f;
	public float airControl = 0.1f;
	public bool autoMove = false;
	public bool buoyant = false;
	public float waterHeight = 1.0f;

	Vector3 mouvementInput;
	bool isGrounded = false;

	void Start()
	{
		cameraController = FindObjectOfType<CameraController>();
		rb = GetComponent<Rigidbody>();
	}

	private void FixedUpdate()
	{
		mouvementInput.z = Input.GetAxisRaw("Vertical") + (autoMove ? 0.5f : 0.0f);
		mouvementInput.x = Input.GetAxisRaw("Horizontal");
		mouvementInput = mouvementInput.normalized * (isGrounded ? 1.0f : airControl);

		if (transform.position.y <= waterHeight && buoyant)
		{
			rb.AddForce(new Vector3(0.0f, -Physics.gravity.y * rb.mass, 0.0f), ForceMode.Force);
		}

		rb.AddForce(cameraController.yRotation * mouvementInput * acceleration * Time.deltaTime, ForceMode.VelocityChange);
		//transform.position = transform.position + mouvementInput * acceleration * Time.deltaTime;

		if (Input.GetKey(KeyCode.Space))// && isGrounded)
			rb.AddForce(new Vector3(0.0f, jumpForce * Time.deltaTime, 0.0f));

		if (Input.GetKeyDown(KeyCode.E))
			rb.velocity = Vector3.zero;
	}

	private void OnCollisionEnter(Collision collision)
	{
		isGrounded = true;   
	}

	private void OnCollisionExit(Collision collision)
	{
		isGrounded = false;
	}
}
