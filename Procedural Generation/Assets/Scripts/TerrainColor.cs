﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainColor : MonoBehaviour
{
    [System.Serializable]
    public struct TerrainColorLayer
    {
        public string label;
        public float height;
        public Color color;
    }

    [SerializeField]
    public List<TerrainColorLayer> colorLayers = new List<TerrainColorLayer>();
}
