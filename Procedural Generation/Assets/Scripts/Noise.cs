﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class Noise
{
	[SerializeField]
	public float seed = 0.0f;
	
	[SerializeField]
	public float amplitude = 1.0f;
	
	[SerializeField]
	public float frequency = 1.0f;

	[SerializeField, Tooltip("Height curve multiplier")]
	AnimationCurve heightCurve;

	public float[] GetHeightMap(int width, int length, float xOffset = 0.0f, float yOffset = 0.0f, float xSize = 1.0f, float ySize = 1.0f)
	{
		float[] heightMap = new float[width * length];
		float xStep = (width + 1.0f) / (float)width;
		float yStep = (length + 1.0f) / (float)length;

		for (int y = 0; y < length; y++)
		{
			for (int x = 0; x < width; x++)
			{
				heightMap[x * width + y] = Mathf.PerlinNoise((x * xStep) / (float)width * frequency + seed + xOffset / (xSize / frequency), (y * yStep) / (float)length * frequency + seed + yOffset / (ySize / frequency)) * amplitude;
				heightMap[x * width + y] += heightCurve.Evaluate(heightMap[x * width + y]);
			}
		}

		return heightMap;
	}

	public void PrintInTexture(Texture2D texture)
	{
		float[] heightMap = GetHeightMap(texture.width, texture.height);
		Color[] pixels = new Color[heightMap.Length];
		for (int i = 0; i < heightMap.Length; i++)
		{
			pixels[i] = new Color(heightMap[i], heightMap[i], heightMap[i],1f);
		}


		texture.SetPixels(pixels);
		texture.Apply();
	}
}