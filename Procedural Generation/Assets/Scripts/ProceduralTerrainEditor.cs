﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(ProceduralTerrain))]
public class ProceduralTerrainEditor : Editor
{
	Vector3 lastPos = Vector3.zero;
	private void OnSceneGUI()
	{
		ProceduralTerrain mesh = (target as ProceduralTerrain);

		if (mesh.transform.position != lastPos)
		{
			lastPos = mesh.transform.position;

			mesh.Generate();
		}
	}
}
