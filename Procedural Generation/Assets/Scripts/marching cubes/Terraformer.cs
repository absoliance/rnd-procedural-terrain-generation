﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MarchingCubes
{
	public class Terraformer : MonoBehaviour
	{
		[SerializeField]
		EndlessTerrain terrain;
		[SerializeField]
		float terraformSize;
		[SerializeField]
		float terraformForce;
		[SerializeField]
		int minHeight;
		[SerializeField]
		int maxHeight;

		// Start is called before the first frame update
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{

			if (Input.GetMouseButtonDown(0))
			{
				RaycastTerraform(true);
			}


			if (Input.GetMouseButtonUp(1))
			{
				RaycastTerraform(false);
			}
		}

		void ModifyOtherChunk(ProceduralTerrain currentChunk, int x, int y, int z, float densityModif)
		{
			Vector3 terrainPos = Vector3.zero;
			if (x <= 0)
				terrainPos.x = -1;
			else if(x >= currentChunk.size)
				terrainPos.x = 1;
			/*if (y <= 0)
				terrainPos.y = -1;
			else if (y >= currentChunk.size)
				terrainPos.y = 1;*/
			if (z <= 0)
				terrainPos.z = -1;
			else if (z >= currentChunk.size)
				terrainPos.z = 1;

			ProceduralTerrainMarchingCubes chunk = (ProceduralTerrainMarchingCubes)terrain.GetChunkByPos(currentChunk.transform.position + terrainPos * currentChunk.totalWidth);
			if (chunk == null) 
				return;
			if (chunk == currentChunk)
				return;
			//x
			if (x == 0)
				x = currentChunk.size;
			else if(x % currentChunk.size == 0)
				x = 0;
			else
			{
				x = x % (currentChunk.size);
				if (x <= 0)
					x += currentChunk.size;
			}
			/*
			//y
			if (y == 0)
				y = currentChunk.size;
			else if (y % currentChunk.size == 0)
				y = 0;
			else
			{
				y = y % (currentChunk.size);
				if (y <= 0)
					y += currentChunk.size;
			}
			*/
			//z
			if (z == 0)
				z = currentChunk.size;
			else if (z % currentChunk.size == 0)
				z = 0;
			else
			{
				z = z % (currentChunk.size);
				if (z <= 0)
					z += currentChunk.size;
			}

			float currentDensity = chunk.GetDensity(x, y, z);
			chunk.ModifyDensity(currentDensity + densityModif, x, y, z);
		}

		void RaycastTerraform(bool create)
		{
			RaycastHit hit;

			if (Physics.Raycast(transform.position, transform.forward, out hit))
			{
				ProceduralTerrainMarchingCubes chunk = hit.collider.gameObject.GetComponent<ProceduralTerrainMarchingCubes>();
				if (chunk == null)
					return;
				Vector3 point = hit.point - chunk.transform.position;
				int hitX = Mathf.RoundToInt(point.x / chunk.totalWidth * (chunk.size + 1));
				int hitY = Mathf.RoundToInt(point.y / chunk.totalWidth * (chunk.size + 1));
				int hitZ = Mathf.RoundToInt(point.z / chunk.totalWidth * (chunk.size + 1));

				for (int x = hitX - Mathf.CeilToInt(terraformSize); x < hitX + Mathf.CeilToInt(terraformSize); x++)
				{
					for (int y = hitY - Mathf.CeilToInt(terraformSize); y < hitY + Mathf.CeilToInt(terraformSize); y++)
					{
						if (y <= minHeight || y >= maxHeight)
							continue;
						for (int z = hitZ - Mathf.CeilToInt(terraformSize); z < hitZ + Mathf.CeilToInt(terraformSize); z++)
						{
							float sqrDist = (hitX - x) * (hitX - x) + (hitY - y) * (hitY - y) + (hitZ - z) * (hitZ - z);
							if (sqrDist < terraformSize * terraformSize)
							{
								float densityModif = -terraformSize * terraformForce / sqrDist;
								if (create)
									densityModif = terraformSize * terraformForce / sqrDist;

								if (x < 0 || y < 0 || z < 0 || x > chunk.size || y > chunk.size || z > chunk.size)
								{
									ModifyOtherChunk(chunk, x, y, z, densityModif);
									continue;
								}
								if (x == 0 || y == 0 || z == 0 || x == chunk.size || y == chunk.size || z == chunk.size)
								{
									ModifyOtherChunk(chunk, x, y, z, densityModif);
								}

								float currentDensity = chunk.GetDensity(x, y, z);
								chunk.ModifyDensity(currentDensity + densityModif, x, y, z);
							}
						}
					}
				}
			}
		}
	}
}
