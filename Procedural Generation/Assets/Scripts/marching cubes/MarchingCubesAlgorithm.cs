﻿using System.Collections;
using Unity.Mathematics;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace MarchingCubes
{
	public class MarchingCubesAlgorithm
	{
		public int chunkSize;
		public float[] densities;
		public float isoValue;
		public int triangleIndex = 0;
		public float totalSize = 1f;

		public Vector3[] vertices;
		public Vector3[] normals;
		public int[] triangles;

		public void Execute()
		{
			vertices = new Vector3[15 * chunkSize * chunkSize * chunkSize];
			normals = new Vector3[15 * chunkSize * chunkSize * chunkSize];
			triangles = new int[15 * chunkSize * chunkSize * chunkSize];
			triangleIndex = -1;

			float step = totalSize / chunkSize;

			//for each voxel

			for (int index = 0; index < chunkSize * chunkSize * chunkSize; index++)
			{
				//get voxel position
				int3 pos = GetVoxelPosition(index);

				//get density of voxel corners
				float[] voxelDensities = GetVoxelDensites(pos);
				
				int totalDensities = CalculateCubeDensities(voxelDensities);

				if (totalDensities <= 0 || totalDensities >= 255)
					continue;
				//if corners densities not 0 or > 255 get edges corresponding;
				int edgeIndex = LookupTables.EdgeTable[totalDensities];


				//get cube corners
				int3[] cubeCorners = GetCubeCorners(pos);

				//get interpolated voxel vertices with density values
				float3[] voxelVertices = GetVoxelVertices(voxelDensities, cubeCorners, edgeIndex);

				int rowIndex = totalDensities * 15;
				//set vertices and triangle with voxel combinaison and interpolated values
				for(int i = 0; i < 15 && LookupTables.TriangleTable[rowIndex + i] != -1; i+=3)
				{
					triangleIndex++;
					vertices[triangleIndex] = voxelVertices[LookupTables.TriangleTable[rowIndex + i + 0]] * step;
					triangles[triangleIndex] = triangleIndex;

					triangleIndex++;
					vertices[triangleIndex] = voxelVertices[LookupTables.TriangleTable[rowIndex + i + 1]] * step;
					triangles[triangleIndex] = triangleIndex;

					triangleIndex++;
					vertices[triangleIndex] = voxelVertices[LookupTables.TriangleTable[rowIndex + i + 2]] * step;
					triangles[triangleIndex] = triangleIndex;

					Vector3 normal = GetNormalFromTriangle(vertices[triangleIndex - 2], vertices[triangleIndex - 1], vertices[triangleIndex]);
					normals[triangleIndex] = normal;
					normals[triangleIndex -1] = normal;
					normals[triangleIndex -2] = normal;
				}
			}
		}

		float3[] GetVoxelVertices(float[] voxelDensities, int3[] cubeCorners, int edgeIndex)
		{
			float3[] voxelVertices = new float3[12];

			for(int i = 0; i < 12; i++)
			{
				if ((edgeIndex & (1 << i)) == 0) { continue; }

				int edgeIndex1 = LookupTables.EdgeIndexTable[i * 2];
				int edgeIndex2 = LookupTables.EdgeIndexTable[i * 2 + 1];

				voxelVertices[i] = InterpolateVoxelEdge(cubeCorners[edgeIndex1], cubeCorners[edgeIndex2], voxelDensities[edgeIndex1], voxelDensities[edgeIndex2], isoValue);
			}

			return voxelVertices;
		}

		static float3 InterpolateVoxelEdge(float3 c1, float3 c2, float d1, float d2, float iso)
		{
			return c1 + (iso - d1) * (c2 - c1) / (d2 - d1);
		}

		int3 GetVoxelPosition(int index)
		{
			int3 pos = new int3();
			pos.x = index / (chunkSize * chunkSize);
			pos.y = index / chunkSize % chunkSize;
			pos.z = index % chunkSize;
			return pos;
		}

		float[] GetVoxelDensites(int3 pos)
		{
			float[] voxelDensities = new float[8];

			for(int i = 0; i < 8; i++)
			{
				voxelDensities[i] = densities[
					//x
					(LookupTables.CubeCornersX[i] + pos.x) * (chunkSize + 1) * (chunkSize + 1) +
					//y
					(LookupTables.CubeCornersY[i] + pos.y) * (chunkSize + 1) +
					//z
					 LookupTables.CubeCornersZ[i] + pos.z
					];
			}
			return voxelDensities;
		}

		int CalculateCubeDensities(float[] voxelCorners)
		{
			if (voxelCorners.Length != 8)
				return -1;

			int value = 0;
			
			for(int i = 0; i < 8; i++)
			{
				if(voxelCorners[i] > isoValue)
					value |= (int)math.pow(2, i);
			}
			return value;
		}

		int3[] GetCubeCorners(int3 pos)
		{
			int3[] cubeCorners = new int3[8];

			for(int i = 0; i < 8; i ++)
			{
				cubeCorners[i] = pos + LookupTables.CubeCorners[i];
			}

			return cubeCorners;
		}

		Vector3 GetNormalFromTriangle(Vector3 v1, Vector3 v2, Vector3 v3)
		{
			Vector3 u = v2 - v1;
			Vector3 v = v3 - v1;

			return new Vector3(
				u.y*v.z - u.z * v.y, 
				u.z*v.x - u.x * v.z, 
				u.x*v.y - u.y * v.x
				);
		}
	}
}