﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralTerrainGenerator : MonoBehaviour
{
	public enum RenderMode : uint
	{
		GRADIENT,
		LAYERS
	}

	[Header("Chunks parameters")]

	[SerializeField, Tooltip("Number of chunks in width")]
	private int width = 1;
	public int Width { get { return width; } }

	[SerializeField, Tooltip("Number of chunks in length")]
	private int length = 1;
	public int Length { get { return length; } }

	[SerializeField, Tooltip("World size of a chunk")]
	private float chunksDimensions = 10.0f;
	public float ChunksDimensions { get { return chunksDimensions; } }

	[SerializeField]
	private GameObject chunkPrefab = null;
	public ObjectPool pool = null;
	public int poolSize = 25;

	[Header("Meshes parameters"), Tooltip("Also see 'NoiseManager' noise(s) parameters")]

	[SerializeField]
	public NoiseManager noiseManager = null;

	[SerializeField, Range(1, 255)]
	private int meshesPrecision = 50;

	public List<ProceduralTerrain> chunks = new List<ProceduralTerrain>();

	[Header("Rendering parameters")]

	[SerializeField]
	public RenderMode renderMode = RenderMode.GRADIENT;

	[SerializeField]
	private Material layersTerrainMaterial;

	[SerializeField]
	private Material gradientTerrainMaterial;

	[SerializeField]
	private TerrainTextureGenerator textureGenerator;

	[SerializeField]
	private Gradient terrainGradient;

	[SerializeField]
	private Texture2D terrainGradientTexture;

	[SerializeField]
	public bool normalizedGradient = true;

	[HideInInspector]
	public Vector2 gradientAmplitude = Vector2.zero;

	[HideInInspector]
	public float UVsMultiplier = 1.0f;

	public void RegenerateChunks()
	{
		foreach (ProceduralTerrain mesh in chunks)
		{
			/*if (mesh == null)
			{
				chunks.Remove(mesh);
				continue;
			}*/

			mesh.Release();
			mesh.totalLength = chunksDimensions;
			mesh.totalWidth = chunksDimensions;
			mesh.size = meshesPrecision;
			mesh.Generate();
			mesh.Generate(); // twice? :thonk:
		}
	}

	public void AddChunk()
	{
		if (pool == null)
			RefreshPool();

		GameObject newMesh = pool.Provide();
		newMesh.transform.parent = transform;

		ProceduralTerrain proceduralMesh = newMesh.GetComponent<ProceduralTerrain>();
		proceduralMesh.terrainMaterial = gradientTerrainMaterial;
		proceduralMesh.noiseManager = noiseManager;
		proceduralMesh.size = meshesPrecision;
		proceduralMesh.totalLength = chunksDimensions;
		proceduralMesh.totalWidth = chunksDimensions;
		proceduralMesh.Generate();

		chunks.Add(proceduralMesh);

		if (chunks.Count > length * width)
		{
			if (width > length)
				length++;
			else
				width++;
		}
	}

	public void RemoveChunk(int index = -1)
	{
		if (transform.childCount <= 0)
			return;

		//if (index <= 0)
		{
			for (int i = transform.childCount - 1; i >= 0; i--)
			{
				if (transform.GetChild(i).gameObject.activeSelf)
				{
					GameObject child = transform.GetChild(i).gameObject;
					chunks.Remove(child.GetComponent<ProceduralTerrain>());
					child.SetActive(false);
					return;
				}
			}
		}
		/*else
		{
			GameObject child = transform.GetChild(index).gameObject;
			chunks.Remove(child.GetComponent<ProceduralMesh>());
			child.SetActive(false);
		}*/
	}

	public void RefreshTexture()
	{
		if (renderMode == RenderMode.GRADIENT)
			RefreshGradientTexture();

		else if (renderMode == RenderMode.LAYERS)
			RefreshLayersTexture();
	}

	private void RefreshLayersTexture()
	{
		if (textureGenerator == null)
			textureGenerator = new TerrainTextureGenerator();

		textureGenerator.material = layersTerrainMaterial;

		foreach (ProceduralTerrain mesh in chunks)
			mesh.terrainMaterial = layersTerrainMaterial;

		textureGenerator.RefreshLayersTextures();
	}

	private void RefreshGradientTexture()
	{
		foreach (ProceduralTerrain mesh in chunks)
			mesh.terrainMaterial = gradientTerrainMaterial;

		terrainGradientTexture = new Texture2D(100, 1);

		for (int i = 0; i < 100; i++)
		{
			terrainGradientTexture.SetPixel(i, 0, terrainGradient.Evaluate((float)i / 100.0f));
		}

		terrainGradientTexture.filterMode = FilterMode.Point;
		terrainGradientTexture.wrapMode = TextureWrapMode.Clamp;
		terrainGradientTexture.Apply();

		Vector2 amplitude = GetAmplitude();


		if (normalizedGradient)
		{
			//gradientTerrainMaterial.SetFloat("UVsMultiplier", 1.0f);
			gradientTerrainMaterial.SetVector("HeightMinMax", amplitude);
		}
		else
		{
			//gradientTerrainMaterial.SetFloat("UVsMultiplier", UVsMultiplier / 100.0f);
			gradientTerrainMaterial.SetVector("HeightMinMax", gradientAmplitude);
		}

		gradientTerrainMaterial.SetTexture("GradientTexture", terrainGradientTexture);
	}

	public Vector2 GetAmplitude()
	{
		if (chunks.Count == 0)
			return Vector2.zero;

		Vector2 amplitude = new Vector2(float.MaxValue, float.MinValue);

		foreach (ProceduralTerrain mesh in chunks)
		{
			amplitude.x = Mathf.Min(amplitude.x, mesh.amplitude.x);
			amplitude.y = Mathf.Max(amplitude.y, mesh.amplitude.y);
		}

		return amplitude;
	}

	public void ComputeChunksNumber()
	{
		if (length * width < chunks.Count)
		{
			int count = chunks.Count - length * width;
			for (int i = 0; i < count; i++)
				RemoveChunk();
		}
		else if (length * width > chunks.Count)
		{
			int count = length * width - chunks.Count;
			for (int i = 0; i < count; i++)
				AddChunk();
		}
	}

	public void ComputeChunksPosition()
	{
		for (int i = 0; i < length; i++)
		{
			float z = i * chunksDimensions;

			for (int j = 0; j < width; j++)
			{
				float x = j * chunksDimensions;

				if (i * width + j < chunks.Count)
					chunks[i * width + j].transform.localPosition = new Vector3(x, 0.0f, z);
			}
		}
	}

	public void RefreshPool()
	{
		Debug.Log("Refreshing chunks pool");

		int count = chunks.Count;
		for (int i = 0; i < count; i++)
			RemoveChunk();

		pool?.Clear(true);

		if (poolSize < length * width)
			poolSize = length * width;

		pool = new ObjectPool(gameObject, chunkPrefab, poolSize);
		foreach (GameObject obj in pool.objectArray)
			obj.transform.SetParent(transform);
	}
}
