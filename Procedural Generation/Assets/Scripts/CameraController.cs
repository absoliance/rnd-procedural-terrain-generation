﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    Transform player;
    [SerializeField]
    Vector3 offset;

    float yRot = 0.0f;
    float xRot = 0.0f;
    Quaternion baseRotation;
    public Quaternion yRotation;
    Quaternion xRotation;
    public float sensitivity = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        baseRotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        yRot += Input.GetAxis("Mouse X");
        xRot -= Input.GetAxis("Mouse Y") * sensitivity;

        yRotation = Quaternion.AngleAxis(yRot, Vector3.up);
        xRotation = Quaternion.AngleAxis(xRot, Vector3.right);
        transform.position = player.position + yRotation * xRotation * offset;
        transform.rotation = yRotation * xRotation * baseRotation;
    }
}
