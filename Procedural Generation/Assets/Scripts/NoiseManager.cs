﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class NoiseManager : MonoBehaviour
{
	[SerializeField]
	private ProceduralTerrainGenerator meshManager = null;

	[SerializeField, Tooltip("Global height curve multiplier")]
	AnimationCurve heightCurve;

	[SerializeField]
	private List<Noise> noises = new List<Noise>();
	public List<Noise> Noises { get { return noises; } }

	public float[] GetHeightMap(int width, int length, float xOffset = 0.0f, float yOffset = 0.0f, float xSize = 1.0f, float ySize = 1.0f)
	{
		float[] map = new float[(width + 1) * (length + 1)];
		for (int i = 0; i < noises.Count; i++)
		{
			float[] currentMap = noises[i].GetHeightMap(width + 1, length + 1, xOffset, yOffset, xSize, ySize);
			for (int j = 0; j < map.Length; j++)
				map[j] += heightCurve.Evaluate(currentMap[j]);
		}

		return map;
	}

	private void OnValidate()
	{
		if (meshManager != null)
		{
			meshManager.RegenerateChunks();
			meshManager.RefreshTexture();
		}
	}

	static public float[] CalculateDensities(float[] map, int size)
	{
		float[] densities = new float[(size + 1) * (size + 1) * (size + 1)];

		for(int x = 0; x < size +1; x++)
		{
			for (int z = 0; z < size + 1; z++)
			{
				for (int y = 0; y < size + 1; y++)
				{
					densities[x * (size + 1) * (size + 1) + y * (size + 1) + z] =
						 map[x * (size + 1) + z] - y;
				}
			}
		}

		return densities;
	}
}
