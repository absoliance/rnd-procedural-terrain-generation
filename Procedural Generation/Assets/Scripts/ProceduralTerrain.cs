﻿using UnityEngine;
using Unity.Collections;
using Unity.Jobs;
using UnityEditor;
using System.Threading;

public class ProceduralTerrain : MonoBehaviour
{
	/*public struct ChunkGeneration : IJob
	{
		public NativeArray<Vector3> vertices;
		public NativeArray<Vector2> uvs;
		public NativeArray<int> triangles;
		public NativeArray<float> heightMap;
		public Vector2 amplitude;
		public float totalWidth;
		public float totalLength;
		public int size;

		public void Execute()
		{
			ComputeVertices();
			ComputeTriangles();
		}

		private void ComputeVertices()
		{
			float wStep = totalWidth / size;
			float lStep = totalLength / size;

			amplitude = new Vector2(float.MaxValue, float.MinValue);

			for (int l = 0; l < size + 1; l++)
			{
				for (int w = 0; w < size + 1; w++)
				{
					vertices[l * (size + 1) + w] = new Vector3(w * wStep, heightMap[l * (size + 1) + w], l * lStep);
					uvs[l * (size + 1) + w] = new Vector2((w * wStep) / totalWidth, (l * lStep) / totalLength);
					amplitude.x = Mathf.Min(amplitude.x, heightMap[l * (size + 1) + w]);
					amplitude.y = Mathf.Max(amplitude.y, heightMap[l * (size + 1) + w]);
				}
			}
		}

		private void ComputeTriangles()
		{
			int t = 0;

			for (int l = 0; l < size; l++)
			{
				for (int w = 0; w < size; w++)
				{
					int i = l * (size + 1) + w;

					triangles[t++] = i;
					triangles[t++] = i + size + 2;
					triangles[t++] = i + 1;

					triangles[t++] = i;
					triangles[t++] = i + size + 1;
					triangles[t++] = i + size + 2;
				}
			}
		}
	}*/

	[Header("Mesh parameters")]

	[SerializeField, Range(1, 255)]
	public int size = 1;

	[SerializeField]
	public float totalWidth = 10.0f;

	[SerializeField]
	public float totalLength = 10.0f;

	[HideInInspector]
	public Vector2 offset = Vector2.zero;

	protected Vector3[] vertices = null;
	protected Vector2[] uvs = null;
	protected int[] triangles = null;
	protected float[] heightMap = null;
	protected Color[] colorMap = null;
	protected float[] densities = null;

	public float[] Densities { get => densities; set { densities = value; isModified = true; } }
	public bool isModified = false;

	[HideInInspector]
	public Vector2 amplitude = new Vector2(float.MaxValue, float.MinValue);

	protected Mesh mesh = null;
	private MeshFilter meshFilter = null;
	private MeshCollider meshCollider = null;
	private MeshRenderer meshRenderer = null;

	[SerializeField]
	public NoiseManager noiseManager = null;

	public Material terrainMaterial = null;
	public bool saveLoadedChunks = true;
	//private Thread generationThread;
	//private ChunkGeneration job;
	//private JobHandle jobHandle;

	protected ProceduralTerrain()
	{
		vertices = new Vector3[256 * 256];
		uvs = new Vector2[256 * 256];
		triangles = new int[255 * 255 * 6];
		isModified = false;
		//generationThread = new Thread(ThreadGenerateMesh);
	}

	private void Awake()
	{
		/*job = new ChunkGeneration()
		{
			vertices = new NativeArray<Vector3>(vertices, Allocator.TempJob),
			uvs = new NativeArray<Vector2>(uvs, Allocator.TempJob),
			triangles = new NativeArray<int>(triangles, Allocator.TempJob),
			heightMap = new NativeArray<float>(noiseManager.GetHeightMap(size, size, offset.x, offset.y, totalWidth, totalLength), Allocator.TempJob),
			size = size,
			totalLength = totalLength,
			totalWidth = totalWidth,
			amplitude = new Vector2(float.MaxValue, float.MinValue)
		};

		jobHandle = job.Schedule();
		jobHandle.Complete();*/
	}

	private void OnApplicationQuit()
	{
		/*job.vertices.Dispose();
		job.uvs.Dispose();
		job.triangles.Dispose();
		job.heightMap.Dispose();*/
	}

	virtual protected void ComputeVertices()
	{
		if (transform.parent != null) // getting noise offset from parent
		{
			offset.y = transform.position.x - transform.parent.position.x;
			offset.x = transform.position.z - transform.parent.position.z;
		}

		heightMap = noiseManager.GetHeightMap(size, size, offset.x, offset.y, totalWidth, totalLength);

		float wStep = totalWidth / size;
		float lStep = totalLength / size;

		amplitude = new Vector2(float.MaxValue, float.MinValue);

		for (int l = 0; l < size + 1; l++)
		{
			for (int w = 0; w < size + 1; w++)
			{
				vertices[l * (size + 1) + w] = new Vector3(w * wStep, heightMap[l * (size + 1) + w], l * lStep);
				uvs[l * (size + 1) + w] = new Vector2((w * wStep) / totalWidth, (l * lStep) / totalLength);
				amplitude.x = Mathf.Min(amplitude.x, heightMap[l * (size + 1) + w]);
				amplitude.y = Mathf.Max(amplitude.y, heightMap[l * (size + 1) + w]);
			}
		}

		ComputeTriangles();
	}

	private void ComputeTriangles()
	{
		int t = 0;

		for (int l = 0; l < size; l++)
		{
			for (int w = 0; w < size; w++)
			{
				int i = l * (size + 1) + w;

				triangles[t++] = i;
				triangles[t++] = i + size + 2;
				triangles[t++] = i + 1;

				triangles[t++] = i;
				triangles[t++] = i + size + 1;
				triangles[t++] = i + size + 2;
			}
		}
	}

	protected void TryGetComponents()
	{
		if (meshFilter == null)
			meshFilter = GetComponent<MeshFilter>();

		if (meshCollider == null)
			meshCollider = GetComponent<MeshCollider>();

		if (meshRenderer == null)
			meshRenderer = GetComponent<MeshRenderer>();
	}

	virtual protected void SetMeshData()
	{
		mesh = new Mesh();
		mesh.SetVertices(vertices, 0, (size + 1) * (size + 1));
		mesh.SetIndices(triangles, 0, size * size * 6, MeshTopology.Triangles, 0);
		mesh.SetUVs(0, uvs, 0, (size + 1) * (size + 1));
		mesh.RecalculateNormals();
	}

	public void Generate()
	{
		if (noiseManager == null)
			return;

		if (mesh == null)
			mesh = new Mesh();

		TryGetComponents();

		ComputeVertices();

		SetMeshData();

		if (meshRenderer != null)
			meshRenderer.material = terrainMaterial;

		if (meshCollider != null)
			meshCollider.sharedMesh = mesh;

		if (meshFilter != null)
			meshFilter.mesh = mesh;
	}

	public void SetPosition(Vector3 pos)
	{
		if (pos == transform.position)
			return;

		transform.position = pos;


		{
			/*if pos dans le dictionnary des meshes modifies
				mesh = dictionnary.GetValueAt(pos)
			else
				GenerateMeshAtPos(pos)*/

			/*if (!EditorApplication.isPlaying || true)
				Generate();
			else
			{
				if (transform.parent != null) // getting noise offset from parent
				{
					offset.y = transform.position.x - transform.parent.position.x;
					offset.x = transform.position.z - transform.parent.position.z;
				}

				if (generationThread.IsAlive)
				{
					generationThread.Abort();
					generationThread = new Thread(ThreadGenerateMesh);
					generationThread.Start();
				}
			}*/

			/*if (jobHandle.IsCompleted)
			{
				vertices = job.vertices.ToArray();
				uvs = job.uvs.ToArray();
				triangles = job.triangles.ToArray();

				mesh = new Mesh();
				mesh.SetVertices(vertices, 0, (size + 1) * (size + 1));
				mesh.SetIndices(triangles, 0, size * size * 6, MeshTopology.Triangles, 0);
				mesh.SetUVs(0, uvs, 0, (size + 1) * (size + 1));
				mesh.RecalculateNormals();

				job.vertices.Dispose();
				job.uvs.Dispose();
				job.triangles.Dispose();
				job.heightMap.Dispose();

				if (meshRenderer == null) meshRenderer = GetComponent<MeshRenderer>();
				if (meshCollider == null) meshCollider = GetComponent<MeshCollider>();
				if (meshFilter == null) meshFilter = GetComponent<MeshFilter>();
				if (meshRenderer != null) meshRenderer.material = terrainMaterial;
				if (meshCollider != null) meshCollider.sharedMesh = mesh;
				if (meshFilter != null) meshFilter.mesh = mesh;

				if (transform.parent != null)
				{
					offset.y = transform.position.x - transform.parent.position.x;
					offset.x = transform.position.z - transform.parent.position.z;
				}

				job = new ChunkGeneration()
				{
					vertices = new NativeArray<Vector3>(vertices, Allocator.TempJob),
					uvs = new NativeArray<Vector2>(uvs, Allocator.TempJob),
					triangles = new NativeArray<int>(triangles, Allocator.TempJob),
					heightMap = new NativeArray<float>(noiseManager.GetHeightMap(size, size, offset.x, offset.y, totalWidth, totalLength), Allocator.TempJob),
					size = size,
					totalLength = totalLength,
					totalWidth = totalWidth,
					amplitude = new Vector2(float.MaxValue, float.MinValue)
				};

				jobHandle = job.Schedule();
				jobHandle.Complete();
			}*/
		}
		Release();
		Generate();
	}

	public void Release()
	{
		densities = null;
		isModified = false;
		heightMap = null;
	}
}