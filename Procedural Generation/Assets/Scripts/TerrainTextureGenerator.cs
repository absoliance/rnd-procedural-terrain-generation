﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TerrainTextureGenerator
{
	[SerializeField]
	public Material material;
	
	[SerializeField, Range(1, 2048)]
	public int resolution;
	
	[SerializeField]
	public List<TerrainLayer> layers;

	public void RefreshLayersTextures()
	{
		Texture2DArray smoothness   = new Texture2DArray(resolution, resolution, layers.Count, TextureFormat.R8, false);
		Texture2DArray occlusion    = new Texture2DArray(resolution, resolution, layers.Count, TextureFormat.R8, false);
		Texture2DArray emissive     = new Texture2DArray(resolution, resolution, layers.Count, TextureFormat.RGB24, false);
		Texture2DArray metallic     = new Texture2DArray(resolution, resolution, layers.Count, TextureFormat.R8, false);
		Texture2DArray albedo       = new Texture2DArray(resolution, resolution, layers.Count, TextureFormat.RGBA32, false);
		Texture2DArray normal       = new Texture2DArray(resolution, resolution, layers.Count, TextureFormat.RGBAFloat, false);
		Texture2D gradient          = new Texture2D(100, 1);

		short begin = 1;

		for (int i = 0; i < layers.Count; i++)
		{
			smoothness.SetPixels(layers[i].GetMap(TerrainLayer.Map.SMOOTHNESS).GetPixels(), i);
			occlusion.SetPixels(layers[i].GetMap(TerrainLayer.Map.OCCLUSION).GetPixels(), i);
			emissive.SetPixels(layers[i].GetMap(TerrainLayer.Map.EMISSIVE).GetPixels(), i);
			metallic.SetPixels(layers[i].GetMap(TerrainLayer.Map.METALLIC).GetPixels(), i);
			albedo.SetPixels(layers[i].GetMap(TerrainLayer.Map.ALBEDO).GetPixels(), i);
			normal.SetPixels(layers[i].GetMap(TerrainLayer.Map.NORMAL).GetPixels(), i);


			int length = layers[i].range - begin;
			Color[] colors = new Color[length];

			for (int j = 0; j < length; j++)
				colors[j] = new Color(i, i, i);

			gradient.SetPixels(begin, 0, length, 1, colors);
			begin = layers[i].range;
		}

		gradient.Apply();
		material.SetTexture("GradientIndexTexture", gradient);

		smoothness.Apply();
		occlusion.Apply();
		emissive.Apply();
		metallic.Apply();
		albedo.Apply();
		normal.Apply();

		material.SetTexture("SmoothnessTextures", smoothness);
		material.SetTexture("OcclusionTextures", occlusion);
		material.SetTexture("EmissiveTextures", emissive);
		material.SetTexture("MetallicTextures", metallic);
		material.SetTexture("AlbedoTextures", albedo);
		material.SetTexture("NormalTextures", normal);
	}
}
