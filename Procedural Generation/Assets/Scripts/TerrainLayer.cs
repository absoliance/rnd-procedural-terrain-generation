﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct TerrainLayer
{
    public enum Map : uint
    {
        SMOOTHNESS,
        OCCLUSION,
        EMISSIVE,
        METALLIC,
        ALBEDO,
        NORMAL,

        COUNT
    }

    [SerializeField]
    public string label;

    [SerializeField, Range(0, 100)]
    public short range;

    [SerializeField]
    private Texture2D smoothness;

    [SerializeField]
    private Texture2D occlusion;

    [SerializeField]
    private Texture2D emissive;

    [SerializeField]
    private Texture2D metallic;

    [SerializeField]
    private Texture2D albedo;

    [SerializeField]
    private Texture2D normal;

    public Texture2D GetMap(Map map)
    {
        Texture2D foundMap = null;

        switch (map)
        {
            case Map.SMOOTHNESS:
                foundMap = smoothness;
                break;
            case Map.OCCLUSION:
                foundMap = occlusion;
                break;
            case Map.METALLIC:
                foundMap = metallic;
                break;
            case Map.EMISSIVE:
                foundMap = emissive;
                break;
            case Map.NORMAL:
                foundMap = normal;
                break;
            case Map.ALBEDO:
                foundMap = albedo;
                break;
            default:
                break;
        }

        if (foundMap == null)
        { 
            foundMap = new Texture2D(2048, 2048);

            for (int i = 0; i < 2048; i++)
                for (int j = 0; j < 2048; j++)
                    foundMap.SetPixel(i, j, Color.black);

            foundMap.Apply();
        }

        return foundMap;
    }
}
