﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

public class EndlessTerrain : MonoBehaviour
{
	[SerializeField]
	private ProceduralTerrainGenerator terrainManager = null;
	private float chunksDimensions = 0.0f;
	private int width = 0;
	private int length = 0;

	[SerializeField]
	private GameObject waterChunkPrefab = null;

	private ProceduralTerrain[] chunks;
	private Vector3[] positions;
	private Dictionary<Vector3, ProceduralTerrain> chunksGrid = new Dictionary<Vector3, ProceduralTerrain>();

	[SerializeField]
	private Player player = null;

	bool needRegenerate = false;
	private Vector3 oldPlayerPos = Vector3.zero;

	private void Start()
	{
		chunksDimensions = terrainManager.ChunksDimensions;
		width = terrainManager.Width;
		length = terrainManager.Length;
		chunks = terrainManager.chunks.ToArray();
		positions = new Vector3[width * length];
		oldPlayerPos = RoundedPlayerPosition();

		if (waterChunkPrefab != null)
		{
			Vector3 seaPos = waterChunkPrefab.transform.position;
			seaPos.x = player.transform.position.x;
			seaPos.z = player.transform.position.z;
			waterChunkPrefab.transform.position = seaPos;
		}

		ComputePositionsGrid();

		for (int i = 0; i < positions.Length; i++)
			chunksGrid.Add(positions[i], chunks[i]);

		for (int i = 0; i < positions.Length; i++)
		{
			chunks[i].SetPosition(positions[i]);

			if (waterChunkPrefab != null)
			{
				Transform water = Instantiate(waterChunkPrefab, chunks[i].transform, false).transform.GetChild(0);
				water.localScale = new Vector3(chunksDimensions / 10.0f, 1.0f, chunksDimensions / 10.0f);
				water.localPosition = new Vector3(chunksDimensions / 10.0f * 5.0f, 0.0f, chunksDimensions / 10.0f * 5.0f);
			}
		}
	}

	private void Update()
	{
		Vector3 roundedPos = RoundedPlayerPosition();

		if (oldPlayerPos.x != roundedPos.x ||
			oldPlayerPos.z != roundedPos.z)
			needRegenerate = true;

		if (!needRegenerate)
			return;

		//Debug.Log("Regenerating...");

		ComputePositionsGrid();
		ComputeChunks();

		needRegenerate = false;
		oldPlayerPos = RoundedPlayerPosition();
	}

	private void ComputePositionsGrid()
	{
		Vector3 playerPos = RoundedPlayerPosition();
		float xOffset = width / 2.0f * chunksDimensions - playerPos.x;
		float zOffset = length / 2.0f * chunksDimensions - playerPos.z;

		for (int i = 0; i < length; i++)
		{
			float z = i * chunksDimensions - zOffset;

			for (int j = 0; j < width; j++)
			{
				float x = j * chunksDimensions - xOffset;
				positions[i * width + j] = new Vector3(x, chunks[i * width + j].transform.position.y, z);
			}
		}
	}

	private void ComputeChunks()
	{
		// Remove not visible chunks from dictionnary
		Vector3[] keys = chunksGrid.Keys.ToArray();
		for (int i = 0; i < keys.Length; i++)
		{
			ProceduralTerrain value;
			chunksGrid.TryGetValue(keys[i], out value);
			if (!IsChunkVisible(value))
				chunksGrid.Remove(keys[i]);
		}

		// Recover not visible chunks and add them to the dictionnary to recompute them
		foreach (Vector3 pos in positions)
		{
			if (!chunksGrid.ContainsKey(pos))
			{
				foreach (ProceduralTerrain chunk in chunks)
				{
					if (!IsChunkVisible(chunk))
					{
						chunksGrid.Add(pos, chunk);
						chunk.SetPosition(pos);
						break;
					}
				}
			}
		}
	}

	private bool IsChunkVisible(ProceduralTerrain chunk)
	{
		foreach (Vector3 pos in positions)
			if (chunk.transform.position == pos)
				return true;

		return false;
	}

	private Vector3 RoundedPlayerPosition()
	{
		Vector3 newPos = player.transform.position;

		newPos.x = newPos.x - (newPos.x % chunksDimensions) - chunksDimensions / 2.0f * (newPos.x >= 0.0f ? -1.0f : 1.0f);
		newPos.z = newPos.z - (newPos.z % chunksDimensions) - chunksDimensions / 2.0f * (newPos.z >= 0.0f ? -1.0f : 1.0f);

		return newPos;
	}

	private Vector3 RoundedPos(Vector3 pos)
	{
		pos.x = pos.x - (pos.x % chunksDimensions);
		pos.z = pos.z - (pos.z % chunksDimensions);
		pos.y = 0;
		return pos;
	}

	public ProceduralTerrain GetChunkByPos(Vector3 pos)
	{
		if(chunksGrid.ContainsKey(pos))
			return chunksGrid[pos];
		return null;
	}
	
	private void OnDrawGizmos()
	{
		if (!EditorApplication.isPlaying)
			return;

		float cd = chunksDimensions / 2.0f;

		for (int i = 0; i < positions.Length; i++)
		{
			Gizmos.color = Color.red;
			Vector3 pos = new Vector3(cd, 50.0f, cd) + positions[i];
			Gizmos.DrawLine(new Vector3(pos.x + cd, 50.0f, pos.z + cd), new Vector3(pos.x + cd, 50.0f, pos.z - cd));
			Gizmos.DrawLine(new Vector3(pos.x + cd, 50.0f, pos.z - cd), new Vector3(pos.x - cd, 50.0f, pos.z - cd));
			Gizmos.DrawLine(new Vector3(pos.x - cd, 50.0f, pos.z - cd), new Vector3(pos.x - cd, 50.0f, pos.z + cd));
			Gizmos.DrawLine(new Vector3(pos.x - cd, 50.0f, pos.z + cd), new Vector3(pos.x + cd, 50.0f, pos.z + cd));
			Gizmos.DrawWireSphere(pos, 8.0f);

			Gizmos.color = Color.blue;
			Gizmos.DrawWireSphere(new Vector3(positions[i].x, 50.0f, positions[i].z), 8.0f);
		}

		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere(player.transform.position, 5.0f);

		Gizmos.color = Color.cyan;
		Gizmos.DrawWireSphere(RoundedPlayerPosition(), 7.0f);
	}
}
