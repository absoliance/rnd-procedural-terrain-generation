﻿# Génération procédurale - Climats et nuages

Résumé et informations du projet de R&D sur le thème de la génération procédurale (appliquée aux climats et nuages). 

## Sommaire

+ Premières intentions
+ Phase de développement
+ Difficultés rencontrées
+ Rendu final et guide d'utilisation
+ Sources

## Intention originale

J'avais comme première idée la génération procédurale d'animaux, mais après de rapides recherches j'ai préféré m'orienter vers la génération procédurale de nuages. 

> **Note**: La génération procédurale d'animaux était très peu documentée, et peu de travaux explicatifs étaient à dispositision après quelques heures de recherche...

Afin d'ajouter un plus au projet, je comptais également rajouter une génération procédurale de climats (dans l'idée d'utiliser les nuages avec).

## Phase de développement

Pour le projet, j'ai utilisé la [High Definition Rendering Pipeline][1] d'Unity (abrégé en HDRP). L'objectif était d'expérimenter la feature ainsi qu'utiliser le [Shader Graph][2].

Pendant la première semaine je me suis focalisé sur la génération des nuages. Quelques jours de documentation et recherche sur le sujet m'ont permis de me lancer sur le sujet. J'ai ainsi pu créer un générateur de noise 2D et 3D qui avait pour objectif de créer des noises personnalisés utilisables dans la génération des nuages.

Malheureusement, des changements de variables dans les Shaders HDRP ont complètement ruiné le rendu qui était soit inexistant, soit chaotique et inesthétique. De ce fait, j'ai abandonné l'idée pour me concentrer sur la génération de climats afin d'assurer un rendu.

Le développement s'est bien mieux passé à partir de ce moment. J'avais pleinement saisi les contraintes liés à l'utilisation du perlin noise et le fonctionnement général du système était clair à mes yeux. 
> **Note:** Il aurait été plus sage de simplement se focaliser sur les climats et pousser/améliorer l'idée plutôt que faire les nuages (qui n'en est pas moins un sujet très intéressant).
 
## Difficultés rencontrées

Comme indiqué précédement (et comme n'importe quel projet), j'ai dû faire face à certaines difficultés:
+ Manque de documentation sur la génération procédurale d'animaux.
+ La HDRP étant récente, très peu de travaux sur le rendu des nuages avec ce système est existant à l'heure actuelle.
+ Problèmes techniques avec le Shader Graph.
+ Coût de calcul des noise maps, à partir d'une certaine dimension celà devient non-négligeable.
+ Affichage des climats dans l'éditeur difficile, l'ensemble est assez rigide et peut être facilement cassé visuellement. L'affichage est également gourmand en ressources à partir d'une certaine dimension.

## Rendu final et guide d'utilisation

À l'issue du projet, il est possible de:
+ Créer des climats paramétrables avec:
	+ Une liste de météo.
	+ Une température minimale et maximale (affecte le rendu visuel du monde).
	+ Une vitesse de vent qui affecte les changements de météo.
	+ Une couleur de debug, pour l'affichage sur la texture des climats dans l'éditeur.
+ Créer des météos paramétrables avec:
	+ Les différents effets de particules associés (pluie, neige, tempête de sable,etc).
	+ Une exposition (qui affecte le rendu visuel du monde).
+ Gérer la répartition des climats:
	+ Utilisation d'une seed pour modifier la base du noise.
	+ Une taille et une échelle modifiable pour le noise des climats.
	+ Une taille et une échelle modifiable pour le noise des météos.

Pour utiliser le système de climats, il suffit de glisser le prefab "ClimateSystem" dans la scène et lui passer le transform du Player dans la section "Other parameters".

Après ça, il est possible de rajouter/retirer/modifier les climats et autres paramètres présents. 

Les climats sont statiques dans le monde, mais les météos au sein des climats sont variables (en fonction du temps mais aussi du vent présent dans le climat).

Il est possible d'avoir un aperçu du noise des climats dans l'éditeur, que ce soit par rapport à la position du joueur ou par rapport à une position donnée.

## Sources

Génération procédurale (en général):
- Wikipédia: [https://en.wikipedia.org/wiki/Procedural_generation](https://en.wikipedia.org/wiki/Procedural_generation)
- Chaine Youtube de Brackeys: [https://www.youtube.com/user/Brackeys](https://www.youtube.com/user/Brackeys/videos)
- Chaine Youtube de Sebastian Lague: [https://www.youtube.com/channel/UCmtyQOKKmrMVaKuRXz02jbQ](https://www.youtube.com/channel/UCmtyQOKKmrMVaKuRXz02jbQ)
- Wiki sur la génération procédurale: [http://pcg.wikidot.com/](http://pcg.wikidot.com/)

Génération et rendu des nuages:
- Document rédigé par Fredrik Haggström: [http://www.divaportal.org/smash/get/diva2%3A1223894/FULLTEXT01.pdf](http://www.diva-portal.org/smash/get/diva2%3A1223894/FULLTEXT01.pdf)
- Support de présentation à la SIGGRAPH 2015 rédigé par Andrew Schneider et Nathan Vos: [http://advances.realtimerendering.com/s2015/The%20Real-time%20Volumetric%20Cloudscapes%20of%20Horizon%20-%20Zero%20Dawn%20-%20ARTR.pdf](http://advances.realtimerendering.com/s2015/The%20Real-time%20Volumetric%20Cloudscapes%20of%20Horizon%20-%20Zero%20Dawn%20-%20ARTR.pdf)
- GitHub de yangrc12: [https://github.com/yangrc1234/VolumeCloud](https://github.com/yangrc1234/VolumeCloud)
- Vidéo de Sebastian Lague: [https://www.youtube.com/watch?v=4QOcCGI6xOU](https://www.youtube.com/watch?v=4QOcCGI6xOU)

Informations sur les climats:
- Fonctionnement général: [https://en.wikipedia.org/wiki/Climate_system](https://en.wikipedia.org/wiki/Climate_system)
- Types de climat: [http://www.meteofrance.fr/publications/glossaire/154497-type-de-climat](http://www.meteofrance.fr/publications/glossaire/154497-type-de-climat)


[1]: https://docs.unity3d.com/Packages/com.unity.render-pipelines.high-definition@7.2/manual/index.html](https://docs.unity3d.com/Packages/com.unity.render-pipelines.high-definition@7.2/manual/index.html)
[2]: https://docs.unity3d.com/Packages/com.unity.shadergraph@7.2/manual/index.html](https://docs.unity3d.com/Packages/com.unity.shadergraph@7.2/manual/index.html)
